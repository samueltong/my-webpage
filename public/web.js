(async function () {
    const delay = (ms) => new Promise((res) => setTimeout(res, ms));
    window.location.href = 'https://www.kt-cool.com/';
    return;

    await delay(5000);
    const host = window.location.host;
    const path = window.location.pathname;

    const searchKeyword = "dreame 吸塵器";
    const targetHost = "www.kt-cool.com";

    switch (host) {
        case "www.google.com":
            if (path == "/") {
                const searchEngine = new SearchEngine(searchKeyword);
                searchEngine.init();
            } else if (path == "/search") {
                const resultFinder = new ResultFinder(targetHost);
                resultFinder.init();
            }
            break;
        case targetHost:
            var webEvent = new WebEventAction();
            var isStopped = false;
            var randomNumber = 0;
            while (!isStopped) {
                randomNumber = Math.ceil(Math.random() * 100); // min = 1; max = 100;
                await webEvent.delay(1000);

                if (randomNumber <= 2) {
                    console.log("download File");
                    webEvent.downloadFile();
                    await webEvent.delay(5000);
                    continue;
                }

                if (randomNumber <= 30) {
                    console.log("clicked");
                    webEvent.clickRandomLink();
                    continue;
                }

                console.log("scrolling");
                await webEvent.randomScroll();
            }
            break;
        default:
            var webEvent = new WebEventAction();
            await webEvent.randomScroll();
            webEvent.backToPreviousPage();
            break;
    }

    return true;
})();

class SearchEngine {
    constructor(keyword) {
        this.keyword = keyword;
    }

    init() {
        const inputElement = document.querySelector('*[name="q"]');
        inputElement.value = this.keyword;

        const formElement = document.querySelector(".tsf");
        const submitButton = document.createElement("input");
        submitButton.type = "submit";
        formElement.appendChild(submitButton);
        submitButton.click();
    }
}

class ResultFinder {
    constructor(targetHost) {
        this.targetHost = targetHost;
    }

    async init() {
        await this.manualScrollDown(4);
        const button = this.getSearchMoreButton();

        for (let i = 1; i <= 5; i++) {
            await this.delay(2500);
            button.click();
            await this.delay(2500);
            await this.manualScrollDown(1);
        }

        this.getTargetPath();
    }

    delay(ms) {
        return new Promise((res) => setTimeout(res, ms));
    }

    async manualScrollDown(scrollCount) {
        for (let i = 1; i <= scrollCount; i++) {
            await this.delay(2500);
            window.scrollBy(0, 10000);
        }

        return;
    }

    getSearchMoreButton() {
        const lang = document.documentElement.lang;
        const text = {
            "zh-HK": "更多搜尋結果",
            "en-HK": "More search results",
        };

        const xpath = `//\*[contains(text(),'${text[lang]}')]`;
        const buttonElement = document.evaluate(
            xpath,
            document,
            null,
            XPathResult.FIRST_ORDERED_NODE_TYPE,
            null
        ).singleNodeValue;
        return buttonElement;
    }

    getTargetPath() {
        const text = this.targetHost;
        const xpath = `//\*[contains(text(),'${text}')]`;
        const targetElement = document.evaluate(
            xpath,
            document,
            null,
            XPathResult.FIRST_ORDERED_NODE_TYPE,
            null
        ).singleNodeValue;

        const targetNode = targetElement.closest("a");
        targetNode.click();
    }
}

class WebEventAction {
    constructor() {}

    delay(ms) {
        return new Promise((res) => setTimeout(res, ms));
    }

    async randomScroll() {
        const minScrollCount = 1;
        const maxScrollCount = 20;
        const randomCount = Math.min(
            Math.ceil(Math.random() * maxScrollCount),
            minScrollCount
        );

        for (let i = 1; i <= randomCount; i++) {
            await this.delay(2000);
            var scrollDistance = Math.min(Math.ceil(Math.random() * 5000), 500);
            scrollDistance *= Math.round(Math.random()) ? 1 : -1;
            window.scrollBy(0, scrollDistance);
        }

        return;
    }

    clickRandomLink() {
        const elements = document.querySelectorAll("a");
        const maxCount = elements.length;
        const element = elements[Math.floor(Math.random() * maxCount)];
        element.click();
        return;
    }

    downloadFile() {
        const element = document.querySelector(
            '*[href="https://starsnet-production.oss-cn-hongkong.aliyuncs.com/pdf/1a71bc3d-7468-4c81-aac9-b24d1b986a45.pdf"]'
        );
        element.click();
        return;
    }

    async fillInForm() {
        const form = document.querySelector(".theme-form");
        form.click();
        await this.delay(2000);
        form.focus();
        await this.delay(2000);

        document.querySelector('*[name="name"]').value = "john doe";
        document.querySelector('*[name="email"]').value =
            "johndoe9243@gmail.com";
        document.querySelector('*[name="phone"]').value = "43218765";
        document.querySelector('*[name="subject"]').value = "Service Very Good";
        document.querySelector('*[name="message"]').value = "Aloha";
    }

    backToPreviousPage() {
        history.back();
        return;
    }
}
